const servicesBtn = document.querySelectorAll(".services-btn");
const servicesItemText = document.querySelectorAll(".service-item-text");

servicesBtn.forEach((btn, index) => {
  btn.addEventListener("click", () => {
    servicesBtn.forEach((elem) => {
      elem.classList.remove("active");
    });
    btn.classList.add("active");

    servicesItemText.forEach((item) => {
      item.classList.remove("active");
    });
    servicesItemText[index].classList.add("active");
  });
});

const amazingWorkMenu = document.querySelector(".amazing-work-menu");
const amazingWorkCategory = document.querySelectorAll(".amazing-work-menu-btn");
const amazingWorkProjects = document.querySelectorAll(
  ".our-amazing-work-projects-item"
);

amazingWorkMenu.addEventListener("click", (evnt) => {
  amazingWorkCategory.forEach((e) => {
    e.classList.remove("active");
  });
  evnt.target.classList.add("active");

  amazingWorkProjects.forEach((elem) => {
    elem.style.display = "none";
    if (elem.dataset.content === evnt.target.dataset.category) {
      elem.style.display = "block";
    } else if (evnt.target.dataset.category === "all") {
      elem.style.display = "block";
    }
  });
});

const feedBackAuthorBigPhoto = document.querySelector(".feedback-author-photo");
const authorList = document.querySelector(".author-list");
const current_icon = document.querySelector(".current-slide");
const next_sliderBttn = document.querySelector(".slider-button-right");
const prev_sliderBtnn = document.querySelector(".slider-button-left");

const slidBtn = document.querySelectorAll(".slider-button");

let fap = document.querySelector(".feedback-author-photo");

const feedbackCitations = [
  `Integer dignissim, augue tempus ultricies luctus, quam dui
  laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar
  odio eget aliquam facilisis. Tempus ultricies luctus, quam dui
  laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar
  odio eget aliquam facilisis.`,
  `Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus?`,
  `Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus? `,
  `Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus?Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus?Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus?`,
  `Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus?`,
  `
  Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus?Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet deleniti sit minus vitae nemo vel voluptatem qui accusantium voluptatibus reiciendis exercitationem nostrum minima error pariatur unde, amet adipisci officia repellendus?`,
];
const feedbackNames = [
  `Michele Dardes`,
  `Martin Laurence`,
  `Hasan Ali`,
  `Nana Arigano`,
  `Bob Larden`,
  `Muhamed Abdul`,
];
const feedbackJobs = [
  `UX Designer`,
  `My Dress Up Darling`,
  `Naruto`,
  `Demon Slayer`,
  `Baki Hanma`,
  `JOJO`,
];

const moveSliderLeft = (index) => {
  document.querySelectorAll(".authors-icons").forEach((elem) => {
    elem.classList.remove("current-slide");
  });
  index--;
  document
    .querySelectorAll(".authors-icons")
    [index].classList.add("current-slide");

  let imgSrc = document.querySelector(".current-slide").children[0].src;
  fap.src = imgSrc;
  document.querySelector(".feedback-text").textContent =
    feedbackCitations[index];
  document.querySelector(".feedback-author").textContent = feedbackNames[index];
  document.querySelector(".feedback-author-job").textContent =
    feedbackJobs[index];
};

const moveSliderRight = (index) => {
  document.querySelectorAll(".authors-icons").forEach((elem) => {
    elem.classList.remove("current-slide");
  });
  index++;
  document
    .querySelectorAll(".authors-icons")
    [index].classList.add("current-slide");

  let imgSrc = document.querySelector(".current-slide").children[0].src;
  fap.src = imgSrc;
  document.querySelector(".feedback-text").textContent =
    feedbackCitations[index];
  document.querySelector(".feedback-author").textContent = feedbackNames[index];
  document.querySelector(".feedback-author-job").textContent =
    feedbackJobs[index];
};

slidBtn.forEach((btn, index) => {
  btn.addEventListener("click", () => {
    btn.classList.add("anim");
    setTimeout(() => {
      btn.classList.remove("anim");
    }, 200);

    let counter;
    document.querySelectorAll(".authors-icons").forEach((elem, indx) => {
      if (elem.classList.contains("current-slide")) {
        counter = indx;
      }
    });

    if (index === 1) {
      if (counter > 2) {
        if (
          !document
            .querySelector(".authors-icons-hidden")
            .hasAttribute("hidden")
        ) {
          if (counter > 4) {
            return;
          } else {
            moveSliderRight(counter);
          }
        } else {
          for (let i = 0; i < 3; i++) {
            document
              .querySelectorAll(".authors-icons")
              [i].classList.add("hide");
          }
          setTimeout(() => {
            for (let i = 0; i < 3; i++) {
              document
                .querySelectorAll(".authors-icons")
                [i].setAttribute("hidden", true);
            }
            document.querySelectorAll(".authors-icons-hidden").forEach((e) => {
              e.removeAttribute("hidden");
              e.classList.add("animation-appear");
            });
            setTimeout(() => {
              document
                .querySelectorAll(".authors-icons-hidden")
                .forEach((e) => {
                  e.classList.remove("animation-appear");
                });
            }, 300);
          }, 300);
        }
      } else {
        moveSliderRight(counter);
      }
    } else {
      if (counter < 1) {
        return;
      } else if (counter < 4) {
        if (!document.querySelector(".authors-icons").hasAttribute("hidden")) {
          if (counter < 1) {
            return;
          } else {
            moveSliderLeft(counter);
          }
        } else {
          document.querySelectorAll(".authors-icons-hidden").forEach((elem) => {
            elem.classList.add("animation-dissap");
          });
          setTimeout(() => {
            document
              .querySelectorAll(".authors-icons-hidden")
              .forEach((elem) => {
                elem.classList.remove("animation-dissap");
                elem.setAttribute("hidden", true);
              });
            for (let i = 0; i < 3; i++) {
              document
                .querySelectorAll(".authors-icons")
                [i].removeAttribute("hidden");
            }
            for (let i = 0; i < 3; i++) {
              document
                .querySelectorAll(".authors-icons")
                [i].classList.replace("hide", "not-hidden");
            }
            setTimeout(() => {
              for (let i = 0; i < 3; i++) {
                document
                  .querySelectorAll(".authors-icons")
                  [i].classList.remove("not-hidden");
              }
            }, 300);
          }, 300);
        }
      } else {
        moveSliderLeft(counter);
        console.log(counter);
      }
    }
  });
});

const authorIcons = document.querySelectorAll(".authors-icons");

authorList.addEventListener("click", (evnt) => {
  if (!evnt.target.closest(".authors-icons")) {
    return;
  }
  let target = evnt.target.closest(".authors-icons");
  authorIcons.forEach((el) => {
    el.classList.remove("current-slide");
  });
  let counter;
  target.classList.add("current-slide");

  document.querySelectorAll(".authors-icons").forEach((elem, indx) => {
    if (elem.classList.contains("current-slide")) {
      counter = indx;
    }
  });

  let imgSrc = target.children[0].src;

  document.querySelector(".feedback-author-photo").src = imgSrc;

  document.querySelector(".feedback-text").textContent =
    feedbackCitations[counter];
  document.querySelector(".feedback-author").textContent =
    feedbackNames[counter];
  document.querySelector(".feedback-author-job").textContent =
    feedbackJobs[counter];
});
